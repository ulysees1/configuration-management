node 'default' {
  package { '7-Zip':
    ensure   => installed,
    provider => 'windows',
  }

  service { 'wuauserv':
    ensure => 'running',
    enable => true,
    provider => 'windows',
  }
}
